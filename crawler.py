#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
"""
usage: crawler.py [--version] [-d|--debug] [-t|--trace=<level>] [--conf=<file>] 
         [-c <name>=<value>] [-h|--help] <command> [<args>...]

options:
   -c <name=value>
   -h, --help
   -t, --trace=<level> [default: error]

The most commonly used crawler.py commands are:
   crawl      crawl documents
   lang       analyse language of documents
   analyse    analyse documents (heavy stuff)
   bing       call bing to get new rssfeeds

See 'crawler help <command>' for more information on a specific command.

"""
from subprocess import call
from docopt import docopt


if __name__ == '__main__':

    args = docopt(__doc__,
                  version='crawler version 1.0',
                  options_first=True)
    # print('global arguments:')
    # print(args)
    # print('command arguments:')

    argv = [args['<command>']] + args['<args>']
    if args['<command>'] in ['crawl', 'lang', 'analyse', 'bing']:
        command = ['python', './src/scripts/crawler_{0}.py'.format(args['<command>']), 'argv={0}'.format(argv[1:])]
        # print command
        exit(call(command))
    elif args['<command>'] in ['help', None]:
        exit(call(['python', './crawler.py', '--help']))
    else:
        exit("%r is not a crawler.py command. See 'crawler --help'." % args['<command>'])


