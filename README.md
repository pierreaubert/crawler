# a simple web crawler designed to work with rssfeeds

## how to use

+ Start sso and rssfeeds
```
cd 7pi/sso
python wsgi.py
```
and
```
cd 7pi/rssfeeds
python wsgi.py
```
+ Start with a sequential crawler (thus slow)
```
cd 7pi/crawler
./src/crawler/scripts/crawler_crawl -l 100 new
```
will crawl 100 new feeds. If you want to refresh feeds, then replace *new* by *refresh*
```
+ Start a faster crawler with redis and celery
```
cd 7pi/crawler
redis-server &
celery --broker='redis://localhost:6379/0' multi start 1 -c 20 -A src/crawler/task
```
check that celery is working:
```
celery --broker='redis://localhost:6379/0' status
```
+ Use a search engine to populate your crawler, if you are intrested in football:
```
python src/crawler/scripts/crawl_bing football
```
+ Automatically deduce language for feeds:
```
python src/crawler/scripts/crawl_lang
```
+ Automatically discover/clusterize feeds:
```
python src/crawler/scripts/crawl_analyse
```
Warning: this is comptational extensive; if you have a lot of documents you will need a cluster of servers.
Currently for:
          - 10000 feeds and 500k rss documents: 
            - need 6h on a core i7, 16GB ram 
            - need 2h on a core i7 extreme, 64 GB ram
          - 1m feeds and 100m rss documents: need 30h on 5x(dual xeon, 6 core each, 64GB ram)
