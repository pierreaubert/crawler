#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

feeds = {
    # -----------------------------------------------------------
    'error': '''
error''',
    # -----------------------------------------------------------
    # a valid atom feed
    # -----------------------------------------------------------
    'atom1': '''
<feed version="0.3" xmlns="http://purl.org/atom/ns#">
<entry>
<title>this is a fake test case</title>
<description>this is a fake test case</description>
</entry>
</feed>''',
    # -----------------------------------------------------------
    # a valid rss feed
    # -----------------------------------------------------------
    'rss1': '''
<rss version="2.0" xmlns:feedburner="http://rssnamespace.org/feedburner/ext/1.0">
<channel>
  <feedburner:browserFriendly>This is an XML content feed. It is intended
    to be viewed in a newsreader or syndicated to another site.
    </feedburner:browserFriendly>
</channel>
</rss>''',
    # -----------------------------------------------------------
    # a valid itunes feed
    # -----------------------------------------------------------
    'itunes1': '''
<rss xmlns:itunes="http://www.itunes.com/DTDs/Podcast-1.0.dtd">
<channel>
<item>
  <itunes:author>Mark Pilgrim</itunes:author>
</item>
</channel>
</rss>''',
    # -----------------------------------------------------------
    # a valid rdf feed
    # -----------------------------------------------------------
    'rdf1': '''
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns="http://purl.org/rss/1.0/">
<channel rdf:about="http://example.com/index.rdf">
  <title>Example feed</title>
</channel>
</rdf:RDF>''',
    # -----------------------------------------------------------
    # a simple feed in english
    # -----------------------------------------------------------
    'atom_crawler_en1': '''
<feed version="0.3" xmlns="http://purl.org/atom/ns#">
<entry>
<title>this is a fake test case</title>
<description>this is a fake test case first one</description>
</entry>
</feed>''',
    # -----------------------------------------------------------
    # a simple feed in english
    # -----------------------------------------------------------
    'atom_crawler_en2': '''
<feed version="0.3" xmlns="http://purl.org/atom/ns#">
<entry>
<title>this is a fake test case</title>
<description>this is a fake test case second one</description>
</entry>
</feed>''',
    # -----------------------------------------------------------
    # a simple feed in french
    # -----------------------------------------------------------
    'atom_crawler_fr1': '''
<feed version="0.3" xmlns="http://purl.org/atom/ns#">
<entry>
<title>ceci est un faux flux</title>
<description>ceci est un premier faux flux</description>
</entry>
</feed>''',
    # -----------------------------------------------------------
    # a simple feed in french
    # -----------------------------------------------------------
    'atom_crawler_fr2': '''
<feed version="0.3" xmlns="http://purl.org/atom/ns#">
<entry>
<title>ceci est un faux flux</title>
<description>ceci est un second faux flux</description>
</entry>
</feed>''',
} 

