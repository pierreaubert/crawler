#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import time
from flask import request, redirect, abort
from flask.views import MethodView
from datas import feeds

class FakeAPI(MethodView):
    """ implement the login API in a restfull manner
    """

    def _get_200(self):
        # id of doc to print
        id = request.args.get('id', 'error')
        return '{0}\n'.format(feeds[id])

    def _get_30X(self, status):
        # simulate a redirect to another doc here
        r_id = request.args.get('to')
        return redirect( '/crawl?status=200&id={0}'.format(r_id))

    def get(self):
        """ return preconstructed datas"""

        # mandatory
        status = int(request.args.get('status'))

        # do we sleep?
        esleep = int(request.args.get('sleep',0))
        if esleep>0:
            time.sleep(esleep)

        # do we have etag or last-modified?

        
        # dispatch per http status
        if status == 200:
            return self._get_200()
        elif status in (301, 302):
            return self._get_3XX(status)
        elif status in (401, 404):
            # bam
            abort(status)
        else:
            abort(500)

        

