#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import logging
from flask import Flask
from apis import FakeAPI

def create_fake():
    app = Flask('fake')
    app.config.from_envvar('CRAWLER_SETTINGS')

    # add views
    app.add_url_rule('/crawl',
                     view_func=FakeAPI.as_view('crawl'),
                     methods=['GET'])

    return app

