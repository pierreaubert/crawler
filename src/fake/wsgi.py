#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from fake.appfake import create_fake

app = create_fake()

# nice for debugging
if __name__ == '__main__':
#    app.run(host=app.config['FAKE_HOST'],
#            port=app.config['FAKE_PORT'],
#            debug=app.config['DEBUG'])

    app.run(host='127.0.0.1', port=9993, debug=True)


