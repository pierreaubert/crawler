#!/usr/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

def acquire_data(command):
    """ ### Three cases are supported:
    1/ test
    2/ loop over items over all feeds
    3/ loop overs all item from 1 particular feeds
    """
    items = None
    if command.command == 'test':
        item1 = {
            'title': 'titre 1 de la france', 
            'content': 'c est le premier item en francais', 
            'description': 'c est le premier item en francais, sa description est relativement longue pour que la detection de la langue fonctionne simplement.', 
        }
        item2 = {
            'title': 'title 2 in united states of america', 
            'content': 'it s the first item in english', 
            'description': 'it s the first item in english, its description is long enough such that automatic language detection works out of the box.', 
        }
        items = [item1, item2]
    elif command.command == 'indexfeeds':
        get_items = "http://{0}:{1}/rssfeeds/feed/{2}/items?limit={3}".format(
            env.TECHRADAR_HOST,
            env.TECHRADAR_PORT,
            command.feed_id[0],
            command.limit)    
        
        r = requests.get(get_items)
        if r.status_code != requests.codes.ok:
            print 'error calling techradar'
            sys.exit(1)
            
            items = r.json()
    elif command.command == 'indexdb':
        logger = logging.getLogger('gensim')
        db = techradar_models.Models(env.DATABASE_URI, logger)
        sql = '''
SELECT
  rssitem_id, feed_id, title, content, description
FROM
  rssitems
ORDER BY
  rowid ASC
LIMIT 
  ?
        ;'''
        items = json.loads(db.json_query(sql,[command.limit]))
        #print json.dumps(items)
        #for item in items:
        #    print 'title:' + item['title']
    else:
        print 'Unknown command, ciao!'

    return items

            
