#!/usr/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

def tokenize_field( item, field ):
    """ tokenize 1 field
    """
    if field in item:
        text = item[field]
        # i do not understand why it can be None from time to time
        if text is not None:
            return [word_tokenize(t) 
                       for t in sent_tokenize(text)]

    return []

        
def tokenize_item(item):
    """ tokenize 1 item and return a flat list of token
    """
    tokens = []

    # get a list of list of token
    tokens.append(tokenize_field(item, 'title'))
    tokens.append(tokenize_field(item, 'content'))
    tokens.append(tokenize_field(item, 'description'))

    # flatten [[[]]] to [] and remove every too short token
    tokens = [t for sl in tokens for ssl in sl for t in ssl if len(t)>2]

    return tokens


def tokens_remove_monooccurrence(tokens):
    """ take a list of lists of tokens and remove single occurrence
    """
    # concatenate all tokens, too slow and use too much mem
    #all_tokens = sum(tokens,[])
    #print '> tokenize: concat' + print_etime()
    # identify all tokens with 1 occurrence only
    # too slow
    #one_tokens = set( word for word in set(all_tokens) 
    #                  if all_tokens.count(word) < 2 )
    dico_tokens = {}
    for slices in tokens:
        for tok in slices:
            if tok not in dico_tokens:
                dico_tokens[tok] = 1
            else:
                dico_tokens[tok] = dico_tokens[tok] + 1
    print '> tokenize look for unique occurence' + print_etime()

    # and remove them
    tokens = [[word for word in token 
               if word in dico_tokens and dico_tokens[word]>1]
              for token in tokens]
    print '> tokenize: and remove them' + print_etime()

    return tokens


@celery.task 
def tokenize_items(items):
    """ tokenize all items
    and remove items with 1 occurence only
    """
    global set_stopwords
    tokens = [] # list of all tokens
    total_length = len(items)
    total = 0
    if 'rssitem_id' in items[0]:
        print '> debug > tokenize_items > first > ' + str(items[0]['rssitem_id'])
    print '> debug > tokenize_items > total > ' + str(total_length)
    # main loop
    for item in items:
        # it can be null because the last slice is not necessary full
        if item is None:
            break
        # get lang for item
        lang = '??'
        if 'description' in item:
            try:
                text = item['description']
                if text is not None:
                    lang = language_likelihood(text)
            except NameError as e:
                print 'Name Error for item:'
            except TypeError as e:
                print 'Type Error for item:'
        # get token list
        lt = tokenize_item(item)

        # remove stopwords if language has been identified
        if lang is not '??':
            lt = [word for word in lt if word not in set_stopwords[lang]]

        # add lt to tokens
        tokens.append(lt)

        # trace where we are
        total = total + 1
        # update_progress( float(total)/total_length) 

    # and hop
    return tokens


def make_shards(n, iterable, fillvalue=None):
    """ shards main array of items in N
    """
    args = [iter(iterable)]*n
    return izip_longest( fillvalue=fillvalue, *args)
    

def tokenize_items_single(items):
    """ tokenize all items
    and remove items with 1 occurence only
    """
    # compute
    tokens = tokenize_items(items)
    # remove mono occurence
    return tokens_remove_monooccurrence(tokens)


def tokenize_items_parallel(items,nshards):
    """ tokenize all items
    and remove items with 1 occurence only
    shard items in N nshards
    """
    shards = []
    shard_sz = len(items)/nshards
    if shard_sz == 0:
        shard_sz = len(items)
    # call in // via celery via a nshards-partition of all items
    job = group( [ tokenize_items.s(ashard)
                   for ashard in make_shards(shard_sz,items) ] )
        
    results = job.apply_async()

    # get all results
    shards = results.join()
    print '> token join() ' + print_etime()

    # ok now merge shards
    tokens = sum(shards,[])
    # return
    return tokens_remove_monooccurrence(tokens)


