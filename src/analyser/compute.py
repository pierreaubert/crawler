#!/usr/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

def compute(command,items):
    """ main compute loop
    """ 
    print '> get data in ' + print_etime()
    print len(items)
    
    # tokenize ---------------------------------------------------------------
    #for item in items:
    #    print str(item['rssitem_id']) + ' ' + str(item['feed_id'])
    if command.parallel:
        tokens = tokenize_items_parallel(items,8)
    else:
        tokens = tokenize_items_single(items)
    dictionary = corpora.Dictionary(tokens)
    dictionary.save('datas/techradar.dict')
    print '> save dictionnary' + print_etime()
    print dictionary
    if command.command == 'test':
        print dictionary.token2id
    print '> tokenize: loop over documents' + print_etime()

    # corpus ---------------------------------------------------------------
    if command.parallel:
        corpus = build_corpus_parallel(items,dictionary,8)
    else:
        corpus = build_corpus_scalar(items,dictionary)
    if command.command == 'test':
        print corpus
    corpora.MmCorpus.serialize('datas/techradar.mm', corpus)
    print '> build corpus ' + print_etime()

    # tfidf  ---------------------------------------------------------------
    tfidf = models.tfidfmodel.TfidfModel(corpus,normalize=True)
    corpus_tfidf = tfidf[corpus]
    corpus_tfidf.save('datas/corpus_tfidf.mm')
    if command.command == 'test':
        for doc in corpus_tfidf:
            print doc
    print '> tfidf' + print_etime()

    # lsi ------------------------------------------------------------------
    lsi = models.lsimodel.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=300)
    corpus_lsi = lsi[corpus_tfidf]
    corpus_lsi.save('datas/corpus_lsi.mm')
    if command.command == 'test':
        for doc in corpus_lsi:
            print doc
    print '> lsi (300 topics)' + print_etime()

    # lsi ------------------------------------------------------------------
    # compute similarity matrix (be carefull of memory here)
    index = similarities.MatrixSimilarity( corpus_lsi )
    index.save('datas/index.mm')
    print '> similarity' + print_etime()
