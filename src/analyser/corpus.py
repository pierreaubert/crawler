#!/usr/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

@celery.task
def build_corpus_scalar(items,dictionary):
    """ loop over all items and build a vector per doc
    """
    corpus = []
    total_length = len(items)
    #total = 0
    if 'rssitem_id' in items[0]:
        print '> debug > corpus_items > first > ' + str(items[0]['rssitem_id'])
    print '> debug > corpus_items > total > ' + str(total_length)
    for item in items:
        if item is None:
            break
        tokens = tokenize_item(item)
        bow = dictionary.doc2bow(tokens)
        corpus.append( bow ) 
        #total = total+1
        #update_progress( float(total)/total_length) 

    return corpus


def build_corpus_parallel(items,dictionary,nshards):
    """ loop over all items and build a vector per doc
    """
    shards = []
    shard_sz = len(items)/nshards
    if shard_sz == 0:
        shard_sz = len(items)
    # call via a group celery task
    job = group( [ build_corpus_scalar.s(shard,dictionary)
                   for shard in make_shards(shard_sz,items) ] )

    results = job.apply_async()

    # pffffff: copy results, hard for RAM and not usefull (TODO)
    shards = results.join()
    print '> corpus join() ' + print_etime()

    # pffffff: idem (TODO)
    corpus = sum(shards, [] )

    return corpus


