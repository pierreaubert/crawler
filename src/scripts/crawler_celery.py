#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from celery import Celery

worker_crawler = Celery('q_crawler',
                        broker='redis://localhost:6379/0',
                        backend='redis://localhost:6379/0',
                        include=['crawler.celery_worker'])

worker_crawler.control.rate_limit( 'crawler.celery_worker.crawl_feeds', '100/s' )

# Optional configuration, see the application user guide.
worker_crawler.conf.update(
    CELERY_TASK_RESULT_EXPIRES=3600,
)

if __name__ == '__main__':
    worker_crawler.start()
