#!/usr/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import sys
import argparse
import time

# get common config param
import techradar_config
env = techradar_config.DevelopmentConfig()
# celery
celery = Celery( 'techradar_gensim' )
celery.config_from_object(env)

# create a empty namespace
class Command(object):
    def __init__(self):
        self.feed_id = None
        #self.limit = 10
        #self.parallel = True
        #self.command = 'indexdb'
        
command = Command()

def print_etime():
    """ print elapse time between 2 calls
    """
    global start_time 
    start = start_time
    now = time.time()
    s = ' {0:1.2f}s '.format(now-start)
    start_time = now
    return s
    

def update_progress(progress,barLength=60):
    """ update_progress() : Displays or updates a console progress bar
    Accepts a float between 0 and 1. Any int will be converted to a float.
    A value under 0 represents a 'halt'.
    A value at 1 or bigger represents 100%
    """
    status = ""
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
        status = "error: progress var must be float\r\n"
    if progress < 0:
        progress = 0
        status = "Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = "Done...\r\n"
    block = int(round(barLength*progress))
    text = "\r: [{0}] {1:02.1f}% {2}".format( 
        "="*block + " "*(barLength-block), progress*100, status)
    sys.stdout.write(text)
    sys.stdout.flush()


if __name__ == '__main__':

    first_time = time.time()
    start_time = first_time

    parser = argparse.ArgumentParser(description='create topics from documents')
    parser.add_argument('--limit', 
                        default=10, 
                        type=int,
                        help='number of items to retrieve')
    parser.add_argument('--parallel', 
                        action='store_true',
                        help='to parallelize items parsing')
    parser.add_argument('--scalar', 
                        action='store_true',
                        help='to parallelize items parsing')
    #group = parser.add_mutually_exclusive_group()
    parser.add_argument('command', 
                        choices=['test','indexdb','indexfeeds'],
                        help='test on 2 items, index all, index feeds')
    parser.add_argument('--feed_id', 
                        metavar='feed_id', 
                        type=int,
                        nargs='+',
                        help='a list of feeds')
    args = parser.parse_args(namespace=command)

    items = analyser.acquire_data(command)
    analyser.compute(command,items)

    start_time = first_time
    print '> finished in ' + print_etime()
    # end ------------------------------------------------------------------
