#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import requests
from os import environ
from sys import exit, argv
from common.get_logger import get_logger
from common.get_client import get_clients
from common.get_env import get_env_from_conf

if __name__ == '__main__':

    # key, 5k req/month free
    acctKey = environ['BING_acctKey']
    if acctKey is None:
        print 'Fatal: an access to bing is mandatory: put it in BING_acctKey shell varo-iable'
        print 'export BING_acctKey="my secret key given to me by microsoft"'
        exit(-1)

    # current api
    bing_api = 'https://api.datamarket.azure.com/Bing/Search/v1/Web?$format=json&Adult=%27Strict%27&WebFileType=%27FEED%27&Query='
    bing_sep='%27'

    # check parameters
    l = len(argv)
    if l<2:
        print argv[0] + ' keywords'
        exit(-1)

    # build request
    kw=argv[1]
    for i in range(2,l):
        kw=kw+'+'+argv[i]

    bing_url = bing_api+bing_sep+kw+bing_sep
    # call bing
    bing = requests.get( bing_url, auth=(acctKey,acctKey) )
    # check result
    if bing.status_code != requests.codes.ok:
        print 'error calling bing' 
        exit(1)
    bing_answers = bing.json()

    env = get_env_from_conf('CRAWLER_SETTINGS')
    logger, raven_c = get_logger(env)
    sso_c, rss_c, crawler_c = get_clients(env,logger)

    # parse bing answer
    if 'd' in bing_answers:
        if 'results' in bing_answers['d']:
            # loop over answer, extract url and add it to database
            for answer in bing_answers['d']['results']:
                url = answer['Url']
                r = rss_c.feed_post(url)
                if r == 1:
                    logger.info(u'OK {0} {1}'.format(kw, answer['Url']))
                elif r == 0:
                    logger.info(u'OK {0} {1} (already there)'.format(kw, answer['Url']))
                else:
                    logger.info(u'KO {0} {1}'.format(kw, answer['Url']))
                
    exit(0)
