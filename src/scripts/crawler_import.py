#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# mass importer of urls
#
# Apache 2 License
# ----------------------------------------------------------------------
"""
usage: crawler import [--help] filename

options:
   -h, --help

"""
import re
from sys import exit, argv
from requests.exceptions import ConnectionError
from crawler.areweconnected import areweconnected
from common.get_logger import get_logger
from common.get_client import get_clients
from common.get_env import get_env_from_conf


# http://daringfireball.net/2010/07/improved_regex_for_matching_urls
match_url="""
(?xi)
\b
(                           # Capture 1: entire matched URL
  (?:
    [a-z][\w-]+:                # URL protocol and colon
    (?:
      /{1,3}                        # 1-3 slashes
      |                             #   or
      [a-z0-9%]                     # Single letter or digit or '%'
                                    # (Trying not to match e.g. "URI::Escape")
    )
    |                           #   or
    www\d{0,3}[.]               # "www.", "www1.", "www2." … "www999."
    |                           #   or
    [a-z0-9.\-]+[.][a-z]{2,4}/  # looks like domain name followed by a slash
  )
  (?:                           # One or more:
    [^\s()<>]+                      # Run of non-space, non-()<>
    |                               #   or
    \(([^\s()<>]+|(\([^\s()<>]+\)))*\)  # balanced parens, up to 2 levels
  )+
  (?:                           # End with:
    \(([^\s()<>]+|(\([^\s()<>]+\)))*\)  # balanced parens, up to 2 levels
    |                                   #   or
    [^\s`!()\[\]{};:'".,<>?«»“”‘’]        # not a space or one of these punct chars
  )
)
"""
match_url_short=r'(?i)\b((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?«»“”‘’]))'


if __name__ == '__main__':

    # get file
    # ------------------------------------------------------------------
    if len(argv) == 2:
        try:
            fd = open(argv[1], "r")
        except IOError as e:
            print 'Can not open "{0}": {1}'.format(argv[1], e.strerror)
            exit(1)
    else:
        print 'Filename to import is mandatory'
        exit(1)

    # get conf
    # ------------------------------------------------------------------
    env = get_env_from_conf('CRAWLER_SETTINGS')
    if env is None:
        exit(1)

    # a nice logger
    # ------------------------------------------------------------------
    logger, raven_c = get_logger(env)

    # check that we have internet connection
    # ------------------------------------------------------------------
    if not areweconnected():
        print """can't connect to famous websites, is internet access on?"""
        exit(-1)

    # iterate over files
    # ------------------------------------------------------------------
    reg = re.compile(match_url_short)
    try:
        # get clients
        sso_c, rss_c, crawler_c = get_clients(env,logger)
        # read file
        for url in fd:
            # check it looks like a url
            if reg.match(url):
                logger.info('Import url: {0}'.format(url))
                rss_c.feed_post(url)
            else:
                logger.info('Reject url: {0}'.format(url))
        
    except ConnectionError as ec:
        print 'Can\'t connect {0}'.format(ec)
        logger.error('Can\'t connect {0}'.format(ec))
        exit(-1)

    # all is well
    exit(0)
    
