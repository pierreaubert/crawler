#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
"""
usage: crawler_lang.py [-v|--verbose] [-l|--limit=<nb>] <command>

options:
    -h, --help            display help
    -v, --verbose         add progress reporting
    -l, --limit=<nb>      nb of feeds to analyse

The most commonly used crawler lang commands are:
    guess                 basic and fast algo to compute language for a feed

See 'crawler_lang.py help <command>' for more information on a specific command.

"""
from sys import exit
from docopt import docopt
from common.get_logger import get_logger
from common.get_client import get_clients
from common.get_env import get_env_from_conf
from language.guess import Guess
from language.update import update_feed

if __name__ == '__main__':

    # get doc
    # ------------------------------------------------------------------
    args = docopt(__doc__,
                  version='crawl version 1.0',
                  options_first=True)
    command = args['<command>']
    if command not in ['guess']:
        print 'unknown command: ' + command
        exit(-1)
    limit = 10
    if '--limit' in args and len(args['--limit'])>0:
        limit = args['--limit'][0]
    verbose = '--verbose' in args or None

    # build guesser
    # ------------------------------------------------------------------
    guesser = Guess()

    # build client
    # ------------------------------------------------------------------
    env = get_env_from_conf('CRAWLER_SETTINGS')
    if not env:
        exit(-1)
    logger, raven_c = get_logger(env)
    current_sso, current_rssfeeds, current_crawler = get_clients(env, logger)
    if not current_crawler:
        exit(-1)

    # get feeds
    # ------------------------------------------------------------------
    command = 'withoutlanguage'
    feeds = current_rssfeeds.crawl(command, limit)
    if feeds is None or len(feeds)==0:
        print 'Nothing ({0}) to crawl, strange!'.format(command)
        exit(0)

    # lunch language guesser and enque all requests
    # ------------------------------------------------------------------
    l = len(feeds)
    for feed in feeds:
        update_feed(current_rssfeeds, guesser, feed['rssfeed_id'])

    # all is well
    exit(0)
    
