#!/usr/bin/env python
#                                                   -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# crawler
# -------
#
# loop over table rssfeeds
#   for each feed
#      put feed in queue
#
# start crawl
#
# while queue not empty
#   get url
#   parse rss via lib
#   if valid
#      insert into database
#
# Apache 2 License
# ----------------------------------------------------------------------
"""
usage: crawler_crawl.py [-v|--verbose] [-b|--behaviour=<do>] [-l|--limit=<nb>] <command>

options:
    -h, --help            display help
    -v, --verbose         add progress reporting
    -b, --behaviour=<do>  do can be scalar, multithreaded, celery, rq
    -l, --limit=<nb>      nb of urls to crawl

The most commonly used crawler crawl commands are:
    new        crawl new documents
    refresh    crawl documents which need to be refreshed
    error      crawl documents with a error status
    redirect   crawl documents with a redirect status

See 'crawler_crawl.py help <command>' for more information on a specific command.

"""
from sys import exit
from docopt import docopt
from common.get_logger import get_logger
from common.get_client import get_clients
from common.get_env import get_env_from_conf
from common.areweconnected import areweconnected
from crawler.celery_worker import crawl_feeds

if __name__ == '__main__':

    # get doc
    # ------------------------------------------------------------------
    args = docopt(__doc__,
                  version='crawl version 1.0',
                  options_first=True)
    command = args['<command>']
    if command not in ['new', 'refresh', 'error', 'redirect']:
        print 'unknown command: ' + command
        exit(-1)
    limit = 10
    if '--limit' in args and len(args['--limit'])>0:
        limit = args['--limit'][0]
    behaviour = 'scalar'
    if '--behaviour' in args and len(args['--behaviour'])>0:
        behaviour  = args['--behaviour'][0]
    if behaviour not in ['scalar', 'multithreaded', 'celery', 'rq']:
        print 'unknown behaviour: ' + behaviour
        exit(-1)
        
    verbose = '--verbose' in args or None

    # check that we have internet connection
    # ------------------------------------------------------------------
    if not areweconnected():
        print """can't connect to famous websites, is internet access on?"""
        exit(-1)

    # build client
    # ------------------------------------------------------------------
    env = get_env_from_conf('CRAWLER_SETTINGS')
    if not env:
        exit(-1)
    logger, raven_c = get_logger(env)
    current_sso, current_rssfeeds, current_crawler = get_clients(env, logger)
    if not current_crawler:
        exit(-1)

    # get feeds
    # ------------------------------------------------------------------
    feeds = current_rssfeeds.crawl(command, limit)
    if feeds is None or len(feeds)==0:
        print 'Nothing ({0}) to crawl, strange!'.format(command)
        exit(0)

    # lunch crawl and enque all requests
    # ------------------------------------------------------------------
    l = len(feeds)
    if 'scalar' in behaviour:
        # loop of feeds one after the others
        for feed in feeds:
            url = feed['url'].rstrip()
            # print u'Crawling {0}'.format(url)
            current_crawler.work(feed)
    elif 'rq' in behaviour:
        print 'Behaviour {0} not yet implemented!'.format(behaviour)
    elif 'celery' in behaviour:
        # batch of sz feeds in 1 job
        sz = 10
        for i in range(0,l/sz):
            # not efficient, slide is a list
            # is isslice faster?
            slice = feeds[i*sz:(i+1)*sz]
            crawl_feeds.delay(env, slice)

        print 'Sending {0} urls to crawler!'.format(l)
    else:
        print 'Behaviour {0} not yet implemented!'.format(behaviour)

    # all is well
    exit(0)
    
