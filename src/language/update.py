#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

fields = ['content', 'title', 'description']


def update_feed(rssfeed_client, guesser, feed_id):
    """ update language for feed_id if above function return only 1 language
    """
    global fields
    lang = '??'
    items = rssfeed_client.feed_get_items(feed_id)
    if items is not None:
        words = []
        for item in items:
            for field in fields:
                words.append(item[field])
        if words:
            words = [unicode(w) for w in words]
            text = u' '.join(words)
            lang = guesser.compute(text)
            
        if '??' not in lang:
            data_feed = {
                'rssfeed_id': feed_id,
                'compute_language': lang
            }
            status = rssfeed_client.feed_put('ok', data_feed)
            print 'Updated language for feed {0} to {1}'.format(feed_id, lang)
            
        
    
