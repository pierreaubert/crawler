#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from nltk import wordpunct_tokenize
from nltk.corpus import stopwords
from iso6391 import languages as isocode


class Guess(object):

    def __init__(self):
        self.fs_stopwords = {}
        for lang in stopwords._fileids:
            self.fs_stopwords[lang] = frozenset(stopwords.words(lang))
        self.isocode = {}
        for (code, names) in isocode:
            snames = names.split(';')
            for name in snames:
                self.isocode[name.lower()] = code

        
    def compute(self, text):
        """ compute a simple probablities of match with a language
        """
        token = wordpunct_tokenize(text)
        lang_likelihood = {}
        for lang in stopwords._fileids:
            lang_likelihood[lang] = len(set(token) & self.fs_stopwords[lang])
        # return highest prob
        lang = sorted(lang_likelihood, key=lang_likelihood.get, reverse=True)[0]
        return self.isocode[lang]


