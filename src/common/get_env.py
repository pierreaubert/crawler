#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import os
import errno

def get_env_from_conf(settings):
    """ get crawler env variables from configuration file"""
    if settings in os.environ:
        conf_file = os.environ[settings]
        conf_env = {}
        try:
            with open(conf_file) as conf:
                exec(compile(conf.read(), conf_file, 'exec'), conf_env)
                if 'env' in conf_env:
                    return conf_env['env']
                else:
                    print 'you must have a env section in your configuration file!'
        except IOError as e:
            if e.errno in errno.ENOENT:
                print '{0} is not a file!'.format(conf_file)
            elif e.errno in errno.EISDIR:
                print '{0} is a directory!'.format(conf_file)
    else:
        print 'You must set env variable settings to point to your configuration file!'
        print 'example: > export {0}=$HOME/src/7pi/crawler/cfg_dev.conf.py'.format(settings)
    return None

