#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import requests
from requests.exceptions import ConnectionError

def areweconnected():
    # can we join some highly available websites?
    # ------------------------------------------------------------------
    ok = 0
    try:
        for ping in ["google.com", "yahoo.com", "microsoft.com"]:
            pong = requests.get('http://www.'+ping)
            if pong.status_code == requests.codes.ok:
                ok = ok+1
    except ConnectionError as re:
        print re
        ok = 0
                
    if ok<2:
        return False
    else:
        return True

