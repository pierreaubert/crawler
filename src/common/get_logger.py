#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
import logging

def get_logger(env):
    """ get a logger """
    logger = logging.getLogger('crawler')
    if 'LOGLEVEL' in env:
        logger.setLevel(env['LOGLEVEL'])
    else:
        logger.setLevel(logging.INFO)
    fh = logging.FileHandler(env['LOGFILE'])
    fm = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    fh.setFormatter(fm)
    logger.addHandler(fh)
    # if using Sentry add a logger
    raven_c = None
    if 'SENTRY_DSN' in env and \
       env['SENTRY_DSN'] is not None and \
       len(env['SENTRY_DSN'])>0:
        # if you want to use Sentry
        from raven import Client as RavenClient
        from raven.conf import setup_logging
        from raven.handlers.logging import SentryHandler
        raven_c = RavenClient(env['SENTRY_DSN'])
        sentry_handler = SentryHandler(raven_c)
        logger.addHandler(sentry_handler)
        setup_logging(sentry_handler)

    # return logger, None
    return logger, raven_c

