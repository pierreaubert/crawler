#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import requests
from urllib3.exceptions import LocationParseError
from socket import timeout as socket_timeout, error as socket_error


def fetch(logger, url, user_agent, etag, last_modified):
    """ get url
    this function crawl an url
    @rtype : a response from request
    @param url: the url to crawl
    @param etag: last etag associated with this url
    @param last_modified: last_modified as returned from the previous http call
    """
    r = None
    status = 500
    try:
        headers = {
            'Accept-Charset': 'utf-8',
            'Accept-Encoding': 'gzip, deflate',
            'ETag': etag,
            'If-Modified-Since': last_modified,
            'User-Agent': user_agent,
        }
        r = requests.get(url, timeout=10, headers=headers)
        status = r.status_code
    except requests.exceptions.MissingSchema as ms:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, ms))
    except requests.exceptions.InvalidSchema as eis:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, eis))
    except requests.exceptions.ConnectionError as ce:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url,ce))
    except requests.exceptions.Timeout as tm:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, tm))
    except requests.exceptions.HTTPError as he:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, he))
    except requests.exceptions.TooManyRedirects as tmr:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, tmr))
    except requests.exceptions.ContentDecodingError as cde:
        logger.info(u'Crawler:Fetcher: decode error {1} on {0}'.format(url, cde))
    except requests.exceptions.ChunkedEncodingError as cde:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, cde))
    except LocationParseError as lpe:
        logger.info(u'Crawler:Fetcher: {1} on {0}'.format(url, lpe))
    except socket_timeout as tm:
        logger.info(u'Crawler:Fetcher: timeout on {0} on {1}'.format(url, tm))
    except socket_error as se:
        logger.info(u'Crawler:Fetcher: socket error on {0} on {1}'.format(url, se))
        
    if status == 500:
        return 500, None
    else:
        return status, r


