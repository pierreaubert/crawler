#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------
from datetime import datetime
from common.get_logger import get_logger
from common.get_client import get_clients
from scripts.crawler_celery import worker_crawler
from celery.exceptions import SoftTimeLimitExceeded

@worker_crawler.task
def crawl_feeds(env, feeds):
    """ crawl a list of feed """
    logger, raven_c = get_logger(env)
    sso_c, rssfeeds_c, crawler_c = get_clients(env, logger)
    try:
        dt_start = datetime.now()
        dt_now = dt_start
        for feed in feeds:
            dt_current = dt_now
            url = feed['url'].rstrip()
            status = crawler_c.work(feed)
            dt_now = datetime.now()
            td = dt_now-dt_current
            logger.info(u'Crawled {status} in {td}: {url}'.format(
                url=url, 
                status=status,
                td=str(td)))
        msg = u'Crawled {sz} feeds in {td}'.format(sz=len(feeds),td=str(td))
        logger.info(msg)
        if raven_c is not None:
            raven_c.captureMessage(msg)
        return True
    except SoftTimeLimitExceeded:
        logger.error(u'Crawler timeout for {0}!'.format(crawl['url']))
        if raven_c is not None:
            raven_c.captureException()
#    except Exception as e:
#        # point to stop with pdb
#        logger.error(u'Crawler unkown error {0}!'.format(e))
#        if raven_c is not None:
#            raven_c.captureException()
    return False
