#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import string
from bs4 import BeautifulSoup
from helpers import time_parser, is_eligible
from urllib import unquote
from .fetcher import fetch
from .parser import parse


class Crawler(object):

    def __init__(self, client, logger):
        self.logger = logger
        self.update_frequency = 60
        self.user_agent = 'https://7pi.eu/crawler.html'
        self.client = client

    def _apply_40X(self, crawl, status, parser):
        """ deal with a deleted status returned by http server
        """
        data = {
            'rssfeed_id': crawl['rssfeed_id'],
            'last_checked': time_parser(None),
            'http_status': status
        }
        return self.client.feed_put('deleted', data)
    
    def _apply_304(self, crawl, status, parser):
        """ deal with a not modified status returned by http server
        """
        data = {
            'rssfeed_id': crawl['rssfeed_id'],
            'last_checked': time_parser(None)
        }
        return self.client.feed_put('updatelastchecked', data)
    
    def _apply_3XX(self, id, status, lastrowid, redirect):
        """ deal with a redirect status returned by http server
        permanent or temporary redirect
        add the redirected in the associated table
        """
        # 2/ mark current as a redirect
        data = {
            'rssfeed_id': id,
            'last_checked': time_parser(None),
            'http_status': status,
            'redirectto': lastrowid,
            'redirecttourl': redirect
        }
        return self.client.feed_put('redirect', data)
    
    
    def _apply_50X(self, crawl, status, parser):
        """ deal with a KO status returned by http server
        no data or parsing failed
        """
        error_data = {
            'rssfeed_id': crawl['rssfeed_id'],
            'last_checked': time_parser(None),
            'http_status': status
        }
        return self.client.feed_put('ko', error_data)
    
    
    def _apply_200(self, crawl, status, response, parser):
        """ deal with a 200 OK from the http server
        """
        # update feed into database
        # print '''DEBUG APPLAY 200> no bozo error'''
        data_feed = {
            'rssfeed_id': crawl['rssfeed_id'],
            'url': response.url,
            'title': parser.feed.get("title"),
            'last_checked': time_parser(None),
            'update_frequency': parser.feed.get("sy_updatefrequency", None),
            'metadata_update': 1,
            'http_status': status,
            'http_etag': response.headers.get('ETag', None),
            'http_last_modified': response.headers.get('Last-Modified', None)
        }
        # post not possible
        self.client.feed_put('ok', data_feed)
    
        # update items into database
        for entry in parser.entries: 
            content = []
            if 'content' in entry:
                for subcontent in entry['content']:
                    if 'value' in subcontent:
                        html = subcontent['value']
                        text = BeautifulSoup(html).get_text()
                        content.append( text )
            item = {
                'rssfeed_id': crawl['rssfeed_id'],
                'title': entry.get('title'),
                'link': entry.get('link'),
                'id': entry.get('id'),
                'published': time_parser(entry.get('published_parsed')),
                'updated': time_parser(entry.get('updated_parsed')),
                'description': entry.get('description'),
                'content': string.join( content )
            }
            # print u'''DEBUG APPLAY 200> POST item {0}'''.format(item['title'])
            self.client.item_post(item)

        # print '''DEBUG APPLAY 200> OK'''
        return True
    
    def work(self, crawl):
        """
        this function fetch an url via requests and then store it in the 
        database by calling the api (and not directly). It would be faster 
        to write directly to the db but on a larger install the database will 
        not be on the same host and it helps to debug the apis.
        """
        if 'url' not in crawl:
            self.logger.error(u'worker called with the wrong structure')
            return -1
        url = crawl['url'].rstrip()
        purl = None
        try:
            purl = unquote(url).decode('utf-8')
        except UnicodeEncodeError as e:
            self.logger.error(u'{0} on {1}'.format(e, url))
            purl = url

        self.logger.info(u'Crawler:Work: Start for: {0}'.format(purl))
                
        # --------------------------
        # 0/ eligibility
        # --------------------------
        if not is_eligible(crawl, self.update_frequency):
            self.logger.info(u'Crawler:Skip: too recent: {0}'\
                             .format(purl))
            return -1

        # --------------------------
        # 1/ crawl
        # --------------------------
        http_etag = None
        if 'http_etag' in crawl:
            http_etag = crawl['http_etag']
        http_last_modified = None
        if 'http_last_modified' in crawl:
            http_last_modified = crawl['http_last_modified']

        http_status, response = fetch(self.logger, 
                                      url,
                                      self.user_agent,
                                      http_etag,
                                      http_last_modified)

        self.logger.info(u'Crawler:Fetch: http status {0} for: {1}'\
                         .format(http_status, purl))
    
        # --------------------------
        # 2/ parse
        # --------------------------
        parser_status = False
        parser = None
        if http_status == 200:
            if 'history' in response and len(response.history)>0:
                print 'hello pdb'
            parser_status, parser = parse(self.logger, 
                                          url, 
                                          http_status, 
                                          response)
            self.logger.info(u'Crawler:Parse: parser status {0} for: {1}'\
                             .format(parser_status, purl))

        # --------------------------
        # 3/ store
        # --------------------------
        store_status = False
        if parser_status is False or \
           http_status in [500, 502, 503, 504]:
            store_status = self._apply_50X(crawl, http_status, parser)
        elif http_status in [304]:
            store_status = self._apply_304(crawl, http_status, parser)
        elif http_status in [300, 301, 302, 303, 305, 307]:
            store_status = self._apply_3XX(crawl, http_status, parser)
        elif http_status in [400, 401, 402, 403, 404, 406, 410]:
            store_status = self._apply_40X(crawl, http_status, parser)
        elif http_status == 200:
            if 'history' in response and len(response.history)>0:
                # history means we have redirect
                # r.url is not necessary in database
                self.client.feed_post(response.url)
                id = self.client.feed_get_id(response.url)
                crawl['rssfeed_id']=id
                store_status = self._apply_200(crawl, http_status, response, parser)
                # now mark feeds in history as redirect
                for rf in r.history:
                    rf_u = rf.url
                    if rf_u == crawl['url']:
                        rf_store_status = self._apply_30X(crawl['rssfeed_id'],
                                                          rf.status_code,
                                                          id,
                                                          rfu)
            else:
                store_status = self._apply_200(crawl, http_status, response, parser)
        else: 
            # error here
            self.logger.error(u'Crawler:Store:Code {0} is not handle: {1}'
                              .format(http_status, purl))

        self.logger.info(u'Crawler:Store: store status {0} for: {1}'\
                         .format(store_status, purl))

        self.logger.info(u'Crawler:Work: Done for: {0}'\
                         .format(purl))
                
        return http_status


