
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from xml.sax import SAXException
from feedparser import parse as fparse
from feedparser import UndeclaredNamespace, CharacterEncodingOverride
from requests.utils import get_unicode_from_response


def parse(logger, url, status, r):
    """ parse a feed
    this function parse with feedparser the data returned by requests
    on success it fills a json array with a feed description and a
    list of items
    @rtype : status, a parsed feed
    @param url: the url to crawl
    @param status: http status
    @param r: a response
    """
    # get url with requests and not with feedparser, store result in feed_data
    feed_parser = None
    feed_data = None

    # call the parser 
    try:
        feed_data = get_unicode_from_response(r)
        feed_parser = fparse(feed_data)
    except SAXException as err:
        logger.error(u'Crawler:Parser: SAX error {1} on {0}'.format(url, err))
    except UndeclaredNamespace as err:
        logger.error(u'Crawler:Parser: Undeclared Namespace  {1} on {0}'.format(url, err))
    except Exception as err:
        logger.error(u'Crawler:Parser: Unknown exception {1} on {0}'.format(url, err))
        
    if not feed_parser:
        logger.error("Failed to parse {0}".format(url))
        return False, None

    # debug trace
    # print r.request.headers
    # print r.headers

    if feed_parser.bozo:
        e = feed_parser.bozo_exception
        if isinstance(e, SAXException):
            msg = e.getMessage()
        else:
            msg = e
        logger.info(u'Crawler:Parser: bozo error {1} on {0}'.format(url, msg))

        # allow some kind of errors (here encoding) which are very common
        if not isinstance(feed_parser.bozo_exception,
                          CharacterEncodingOverride):
            # can we recover something with bs4?
            # TODO
            return False, None

    # code then parser
    return True, feed_parser

