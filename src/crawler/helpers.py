#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

import time
from datetime import datetime, timedelta

def time_parser(time_str):
    """
    This function parses the time str from feedparser. Feeds may not
    always include the time for update, and published. So if it is
    in the feed the current time will be set for those values.

    :param time_str The time str to be parsed.

    :return UNIX timestamp
    @rtype : time
    """

    try:
        if time_str is not None:
            return time.mktime(time_str)
    except ValueError:
        # i get "out of range" from time to time
        pass

    return time.time()
    

def is_new_feed(crawl):
    """ define in the url is already in the DB or not
    """
    if 'http_status' in crawl and crawl['http_status'] != 0:
        return False
    else:
        return True


def is_eligible(crawl, update_frequency):
    """
    define the criteria to refresh the feed
    """
    # if new, the crawl anyway
    if is_new_feed(crawl):
        return True
        
    if 'last_checked' in crawl:
        last_checked = crawl['last_checked']
            
        # how many minutes between now and last_checked
        if last_checked:
            t0 = datetime.strptime(last_checked, "%a, %d %b %Y %H:%M:%S %Z")
            t1 = datetime.utcnow()
            t_delta = t1-t0-timedelta(minutes=update_frequency)
            if t_delta.total_seconds() > 0:
                return True
            else:
                return False
        else:
            return False

    return True


