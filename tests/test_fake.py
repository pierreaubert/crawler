#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# Apache 2 License
# ----------------------------------------------------------------------

import unittest
from fake.appfake import create_fake
from fake.datas import feeds

class FakeTests(unittest.TestCase):

    def setUp(self):
        self.server = create_fake()
        self.server.testing = True

    def tearDown(self):
        pass

    def test_status200(self):
        """ let see if the schema as been uploaded
        """
        with self.server.test_client() as tc:
            r = tc.get('/crawl?status=200&id=atom1')
            self.assertEqual(r.status_code, 200)
            self.assertIn(feeds['atom1'], r.data)

    def test_status404(self):
        """ let see if the schema as been uploaded
        """
        with self.server.test_client() as tc:
            r = tc.get('/crawl?status=404&id=atom1')
            self.assertEqual(r.status_code, 404)

        
if __name__ == '__main__':
    unittest.main()
