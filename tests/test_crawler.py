#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Author: P. Aubert pierreaubert@yahoo.fr
#
# Apache 2 License
# ----------------------------------------------------------------------

import os
import signal
import unittest
import time
from exceptions import IOError
from subprocess import Popen
 
# load crawler helpers
from common.get_logger import get_logger
from common.get_client import get_clients
from common.get_env import get_env_from_conf
# from common.areweconnected import areweconnected


def try_remove_dbfile(fname):
    try:
        if os.access(fname, os.F_OK):
            os.unlink(fname)
        else:
            print '''Can't access file {0}!'''.format(fname)
    except IOError as ex:
        print '''Can't open/close db file {0}!'''.format(ex)

# -----------------------------------------------------------------------------
#
# test Crawler
#
# -----------------------------------------------------------------------------
class CrawlerTests(unittest.TestCase):
    """ all tests on Crawler: both model and api """

    def setUp(self):
        """ create app """
        env = dict(os.environ)
        env['SSO_SETTINGS'] = '/home/pierre/dev/7pi/sso/cfg_test.conf.py'
        env['RSSFEEDS_SETTINGS'] = '/home/pierre/dev/7pi/rssfeeds/cfg_test.conf.py'
        env['CRAWLER_SETTINGS'] = '/home/pierre/dev/7pi/crawler/cfg_test.conf.py'
        env['PYTHONPATH'] = '{aubergine}:{sso}:{rssfeeds}:{crawler}:{userfeeds}'\
            .format(aubergine='/home/pierre/dev/7pi/aubergine/src',
                    sso='/home/pierre/dev/7pi/sso/src',
                    rssfeeds='/home/pierre/dev/7pi/rssfeeds/src',
                    crawler='/home/pierre/dev/7pi/crawler/src',
                    userfeeds='/home/pierre/dev/7pi/userfeeds/src')
        command_sso = ['/usr/bin/python', 
                       '/home/pierre/dev/7pi/sso/wsgi.py']
        command_rssfeeds = ['/usr/bin/python', 
                            '/home/pierre/dev/7pi/rssfeeds/wsgi.py']
        command_fake = ['/usr/bin/python', 
                        '/home/pierre/dev/7pi/crawler/src/fake/wsgi.py']
        try:
            self.p_sso = Popen(command_sso, env=env, preexec_fn=os.setsid)
            self.p_fake = Popen(command_fake, env=env, preexec_fn=os.setsid)
            # let time at sso to boot, rssfeeds will connect to it and may bailed out
            # if sso is not yet started, should spin a bit in rssfeeds
            time.sleep(2)
            self.p_rssfeeds = Popen(command_rssfeeds, env=env, preexec_fn=os.setsid)
        except OSError as e:
            print e
            exit(-1)

        crawler_env = get_env_from_conf('CRAWLER_SETTINGS')
        if not crawler_env:
            exit(-1)
        self.logger, self.raven_c = get_logger(crawler_env)
        time.sleep(2)
        self.current_sso, self.current_rssfeeds, self.current_crawler \
            = get_clients(crawler_env, self.logger)
        if not self.current_sso or not self.current_rssfeeds:
            exit(-1)

        # print 'PID SSO: {0}'.format(self.p_sso.pid)
        # print 'PID Fake {0}'.format(self.p_fake.pid)
        # print 'PID Rss  {0}'.format(self.p_rssfeeds.pid)

    def tearDown(self):
        """ kill servers and remove db file if any"""
        #
        os.killpg(self.p_sso.pid, signal.SIGTERM)
        os.killpg(self.p_rssfeeds.pid, signal.SIGTERM)
        os.killpg(self.p_fake.pid, signal.SIGTERM)
        # 
        try_remove_dbfile('/home/pierre/tmp/sso_test.db')
        try_remove_dbfile('/home/pierre/tmp/rssfeeds_test.db')

    def test_smoketest_subservers(self):
        """ let see if subprocess have been spawned
        """
        self.assertIs(self.p_sso.poll() , None)
        self.assertIs(self.p_rssfeeds.poll() , None)

    def test_controller_smoketest(self):
        """ check if the controller can add 2 feeds """
        news = [ 'http://127.0.0.1:9993/crawl?status=200&id=atom_crawler_en1',
                 'http://127.0.0.1:9993/crawl?status=200&id=atom_crawler_en2' ]

        # send feed into rssfeeds
        for feed in news:
            self.current_rssfeeds.feed_post(feed)

        # get them back
        feeds = self.current_rssfeeds.crawl('new', 2)
        self.assertIsNot(feeds, None)
        print feeds
        self.assertEqual( len(feeds), 2 )
        for feed in feeds:
            self.assertTrue( feed['url'] in news)
            
        # crawl them
        for feed in feeds:
            self.current_crawler.work(feed)
        
        # check that we have nothing else to crawl
        feeds = self.current_rssfeeds.crawl('new', 2)
        self.assertIsNot(feeds, None)
        self.assertTrue( len(feeds)==0 )

        # check what's in database
        rfeeds = self.current_rssfeeds.crawl('refresh', 2)
        for feed, rfeed in zip(feeds, rfeeds):
            self.assertIn( feed['url'], rfeed['url'] )

        # crawl them again
        for feed in rfeeds:
            self.current_crawler.work(feed)
        
        # check what's in database
        r2feeds = self.current_rssfeeds.crawl('refresh', 2)
        for feed, r2feed in zip(feeds, r2feeds):
            self.assertIn( feed['url'], r2feed['url'] )



if __name__ == '__main__':
    unittest.main()

# stupid keyboard
# [] {} / ~ ()
