#!/usr/bin/env python
#                                                  -*- coding: utf-8 -*-
# Author: Pierre F. Aubert                  pierreaubert at yahoo dot fr
# ----------------------------------------------------------------------

from os import environ
import logging
from datetime import date

HOME='{0}/tmp'.format(environ['HOME'])

env = {
    # general
    'VERSION': '0.3',
    'DEBUG': True,
    'TESTING': False,
    # main log file
    'LOGFILE': '{0}/crawler_dev_{1}.log'.format(HOME, date.today().isoformat()),
    # where is the SSO
    'SSO_URL': 'http://127.0.0.1:9090',
    'SSO_CRAWLER_LOGIN': 'crawler',
    'SSO_CRAWLER_PASSWORD': 'crawler2013',
    # where is the feed server
    'RSSFEEDS_URL': 'http://127.0.0.1:9091',
    # do we use sentry?
    'SENTRY_DSN': None,
}

